from odoo import models, fields, api


class WizardPartner(models.TransientModel):
    _name = 'wizard.partner'
    _description = 'Create partner card'

    name = fields.Char(string="name", required=True)
    is_company = fields.Boolean(string="Is Company")

    def create_wizard_partner(self):
        return {
            'name': 'create wizard partner',
            'view_mode': 'form',
            'res_model': 'res.partner',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'context': {'default_name': self.name, 'default_is_company': self.is_company},
            'domain': [('name', '=', self.name)]
        }
