from odoo import api, fields, models


class FirstModel(models.Model):
    _inherit = ["mail.thread", "mail.activity.mixin"]
    _name = "first.model"
    _description = "First Model"

    name = fields.Char(string='Name', required=True)
    age = fields.Integer(string='Age')
    gender = fields.Selection(selection=[
        ('male', 'Male'),
        ('female', 'Female'),
        ('other', 'other')
    ], string="", required=True, default='male')
    description = fields.Text(string='Description')
    user_create_date = fields.Date(string='Date', required=True)
