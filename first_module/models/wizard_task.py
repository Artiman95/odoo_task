from odoo import models, fields, api


class WizardTask(models.TransientModel):
    _name = 'wizard.task'
    _description = 'Create Contact Card Wizard'

    name = fields.Char(string="name")
    is_company = fields.Boolean(string="Is Company")

    def action_wizard_task(self):
        print("Button Is Clicked")

    def create_person(self):
        return {
            'name': 'create person',
            'view_mode': 'form',
            'res_model': 'res.partner',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'context': {'default_name': self.name, 'default_is_company': self.is_company}
        }

    def create_company(self):
        return {
            'name': 'create company',
            'view_mode': 'form',
            'res_model': 'res.partner',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'context': {'default_name': self.name, 'default_is_company': self.is_company}
        }

    def create_partner(self):
        return {
            'name': 'create partner',
            'view_mode': 'form',
            'res_model': 'wizard.partner',
            'type': 'ir.actions.act_window',
            'target': 'new'
        }

    def create_partner_popup(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Contact Card',
            'view_mode': 'form',
            'res_model': 'res.partner',
            'res_id': partner.id,
            'target': 'current',
        }
