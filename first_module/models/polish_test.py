from odoo import api, fields, models


class PolishTest(models.Model):
    _name = "polish.test"
    _description = "Polish Test"

    text = fields.Char(string='text')
    check1 = fields.Boolean(string='Test 1')
    check2 = fields.Boolean(string='Test 2')
    check_all = fields.Boolean(string='Select all')
    select1 = fields.Selection(selection=[('1', '1'), ('2', '2'), ('3', '3')], string='Select 1')
    select2 = fields.Selection(selection=[('4', '4'), ('5', '5'), ('6', '6')], string='Select 2')
    boolean1 = fields.Boolean(string='Boolean 1')
    boolean2 = fields.Boolean(string='Boolean 2')
    boolean3 = fields.Boolean(string='Boolean 3')
    boolean4 = fields.Boolean(string='Boolean 4')
    boolean5 = fields.Boolean(string='Boolean 5')
    boolean6 = fields.Boolean(string='Boolean 6')
    boolean7 = fields.Boolean(string='Boolean 7')
    boolean8 = fields.Boolean(string='Boolean 8')
    boolean9 = fields.Boolean(string='Boolean 9')

    @api.onchange('check1')
    def onchange_check1(self):
        if self.check1:
            if not self.text:
                self.text = "[Test 1]"
            else:
                self.text += " [Test 1]"
        else:
            self.text = ""
            # self.text = self.text.replace("[Test 1]", "").strip()

    @api.onchange('check2')
    def onchange_check2(self):
        if self.check2:
            if not self.text:
                self.text = "{Test 2}"
            else:
                self.text += " {Test 2}"
        else:
            self.text = ""
            # self.text = self.text.replace("{Test 2}", "").strip()

    @api.onchange('check_all')
    def all_check(self):
        if self.check_all == True:
            self.check1 = True
            self.check2 = True
        else:
            self.check1 = False
            self.check2 = False
