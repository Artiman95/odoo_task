# -*- coding: utf-8 -*-
{
    'name': 'first module task1',
    'version': '14.0.1.0',
    'summary': 'First Module Software',
    'sequence': -100,
    'description': """First Module Software""",
    'category': 'Productivity',
    'website': 'https://www.google.com',
    'depends': ["base", "mail", "sale", "contacts"],
    'data': [
        'security/ir.model.access.csv',
        'wizard/wizard_partner_view.xml',
        'views/express_delivery_view.xml',
        'views/express_stock_picking_view.xml',
        'views/is_primary_view.xml',
        'views/wizard_view.xml',
        'views/polish_test_view.xml',
        'views/first_model_views.xml'
    ],
    'demo': [],
    'qweb': [],
    'installable': True,
    'application': True,
    'auto_install': False,
}
