from odoo import api, fields, models


class ExpressDelivery(models.Model):
    _inherit = 'sale.order.line'

    express = fields.Boolean(string="Express Delivery")

    def _prepare_procurement_values(self, group_id=False):
        res = super(ExpressDelivery, self)._prepare_procurement_values(group_id)
        res.update({'express': self.express})
        return res

# class StockPicking(models.Model):
#     _inherit = 'stock.picking'
#
#     @api.model
#     def create(self, vals):
#         return 1 / 0
