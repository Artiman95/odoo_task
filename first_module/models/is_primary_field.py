from odoo import api, fields, models


class Partner(models.Model):
    _inherit = 'res.partner'

    is_primary = fields.Boolean(string="Is Primary Contact")

    @api.onchange('is_primary')
    def _onchange_is_primary(self):
        partner_ids = self._origin.parent_id.child_ids.filtered(lambda rec: rec.is_primary)
        if partner_ids:
            self.update({'is_primary': True})
            partner_ids.update({'is_primary': False})
        else:
            partner_ids.update({'is_primary': True})
