from odoo import api, fields, models


class SalesOrderTask(models.Model):
    _inherit = 'sale.order.line'

    product_id = fields.Many2one('product.product')

    @api.onchange('product_id')
    def onchange_product_id(self):
        product_ids = self.order_id.order_line.mapped('product_id.id')
        domain = [('id', 'not in', product_ids)]
        return {'domain': {'product_id': domain}}
