from odoo import fields, models, api


class StockMoveInherit(models.Model):
    _name = 'stock.picking'
    _inherit = 'stock.picking'
    express = fields.Boolean(string="Express Delivery", default=False)
